The following scripts plot the results given in the report:
1) plotSampleStats.py - plot the total short read alignment distributions, as presented in chapter 4.1
2) plotSampleStatsBoxplots.py - plot the quartile short read alignment distributions, as presented in chapter 4.2 and 4.3
3) plotSampleExtractSR.py - plot the classification pie chart for Short read / no Shortread associated miRNAs, as presented in chapter 4.4
4) plotSampleBPgroupBoxplots.py - plot the grouping of base-pairing in 5' end of 5' mature sequence as boxplots, as presented in chapter 4.5
5) plotSampleSRvsFUll.py - plot short read expresion levels vs mature sequence expression levels, as presented in chapter 4.6

Additionally, the statistical methods used to produce the results presented in chapter 4.3 are found in the subfolder 'scripts for statistical methods'.



Input files:
	GCF data:					
		miRNA01_alignments.txt		DOC KO
		miRNA02_alignments.txt		DOC KO
		miRNA03_alignments.txt		DOC KO
		miRNA04_alignments.txt		DOC WT
		miRNA05_alignments.txt		DOC WT
		miRNA06_alignments.txt		DOC WT
		miRNA07_alignments.txt		GH KO
		miRNA08_alignments.txt		GH KO
		miRNA09_alignments.txt		GH KO
		miRNA10_alignments.txt		GH WT
		miRNA11_alignments.txt		GH WT
		miRNA12_alignments.txt		GH WT

	GSE data:
		SRR797059_alignments.txt	Ago1 IP
		SRR797060_alignments.txt	Ago2 IP
		SRR797061_alignments.txt	Ago3 IP
		SRR797062_alignments.txt	Ago4 IP
		SRR797063_alignments.txt	Control IP

	Reference input files:
		hsa_reference_new.txt		Human reference hairpins
		hsa_folds_new.txt			Human miRNA fold structures
		hsa_matures_new.txt			Human mature miRNA references
		mmu_references_new.txt		Mouse reference hairpins
		mmu_folds_new.txt			Mouse miRNA fold structures
		mmu_matures.txt				Mouse mature miRNA references