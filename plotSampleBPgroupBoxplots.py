import shortreadProcesser as sp
import matplotlib as mpl 
mpl.use('Agg')
import matplotlib.pyplot as plt 
import math
import numpy

def main():
	Ago1_hairpins, Ago1_matures, Ago1_shortreads = sp.produceShortreadsAndMatures('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797059_alignments.txt', 'SRR797059')
	Ago2_hairpins, Ago2_matures, Ago2_shortreads = sp.produceShortreadsAndMatures('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797060_alignments.txt', 'SRR797060')
	Ago3_hairpins, Ago3_matures, Ago3_shortreads = sp.produceShortreadsAndMatures('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797061_alignments.txt', 'SRR797061')


	dataTotal = findGroupsTotal([Ago1_matures, Ago2_matures, Ago3_matures], [Ago1_hairpins, Ago2_hairpins, Ago3_hairpins], [Ago1_shortreads, Ago2_shortreads, Ago3_shortreads], ['Ago1', 'Ago2', 'Ago3'])
	shortreads1 = dataTotal[0:4]
	shortreads2 = dataTotal[4:]

	plotBPboxplots(shortreads1, shortreads2, 'Ago1 IP, Ago2 IP and Ago3 IP')


def plotBPboxplots(shorties1, shorties2, samp):
	"""
	Plots boxplots for the different BP groups given.
	shorties is a list of shortread-lists
	names is a list of the names of the groups the shortread lists belong to.
	"""

	#return 
	start3p1, start5p1, end3p1, end5p1 = list(), list(), list(), list()

	for shortie in shorties1:
		news5, news3, newe5, newe3 = createBPData(shortie)
		start5p1.append(news5)
		start3p1.append(news3)
		end5p1.append(newe5)
		end3p1.append(newe3)

	#print 'Number of 5p endreads for 5p paired is %d for sample %s' % (len(end5p1[0]), samp)

	fig1 = plt.figure(figsize=(15,10))
	# Mature sequence start bp groups - without fliers
	ax1 = fig1.add_subplot(111)
	names1 = ['5p paired', '3p paired', '5p and 3p paired', '5p and 3p unpaired']
	ind1 = numpy.arange(len(names1))
	width = 0.1

	plt.xlim([min(ind1) - 1, max(ind1) + 1])
	
	bp_start5 = ax1.boxplot(start5p1, positions=ind1, patch_artist=True, widths=width, sym='')
	bp_end5 = ax1.boxplot(end5p1, positions=ind1+width, patch_artist=True, widths=width, sym='')
	bp_start3 = ax1.boxplot(start3p1, positions=ind1+2*width, patch_artist=True, widths=width, sym='')
	bp_end3 = ax1.boxplot(end3p1, positions=ind1+3*width, patch_artist=True, widths=width, sym='')

	
	for box in bp_start5['boxes']:
		box.set(color='#2166ac', facecolor='#92c5de')
	for box in bp_end5['boxes']:
		box.set(facecolor='#f4a582', color='#b2182b')
	for box in bp_start3['boxes']:
		box.set(color='#762a83', facecolor='#c2a5cf')
	for box in bp_end3['boxes']:
		box.set(facecolor='#a6dba0', color='#1b7837')

	for whisk in bp_start5['whiskers']:
		whisk.set(color='#2166ac', linewidth=2)
	for whisk in bp_end5['whiskers']:
		whisk.set(color='#b2182b', linewidth=2)
	for whisk in bp_start3['whiskers']:
		whisk.set(color='#762a83', linewidth=2)
	for whisk in bp_end3['whiskers']:
		whisk.set(color='#1b7837', linewidth=2)


	for median in bp_start5['medians']:
		median.set(color='#7a0177', linewidth=2)
	for median in bp_end5['medians']:
		median.set(color='#7a0177', linewidth=2)
	for median in bp_start3['medians']:
		median.set(color='#7a0177', linewidth=2)
	for median in bp_end3['medians']:
		median.set(color='#7a0177', linewidth=2)

	for cap in bp_start5['caps']:
		cap.set(color='#2166ac', linewidth=2)
	for cap in bp_end5['caps']:
		cap.set(color='#b2182b', linewidth=2)
	for cap in bp_start3['caps']:
		cap.set(color='#762a83', linewidth=2)
	for cap in bp_end3['caps']:
		cap.set(color='#1b7837', linewidth=2)

	ax1.set_xticks(ind1, minor=False)
	ax1.set_xticklabels(names1, fontsize=20 ,minor=False, fontdict=False, rotation=20, ha='right')
	ax1.tick_params(axis='y', labelsize=20)
	#ax1.set_title(samp + ' basepair groups', fontsize=20)
	ax1.set_ylabel('log$_2$(#shortreads/#mature sequences)', fontsize=20)
	ax1.spines['top'].set_visible(False)
	ax1.spines['right'].set_visible(False)
	ax1.xaxis.set_ticks_position('bottom')
	ax1.yaxis.set_ticks_position('left')
	start5Rect = mpl.patches.Rectangle([0.025, 0.05], 0.05, 0.1, ec='#2166ac', fc='#92c5de')
	end5Rect = mpl.patches.Rectangle([0.025, 0.05], 0.05, 0.1, ec='#b2182b', fc='#f4a582')
	start3Rect = mpl.patches.Rectangle([0.025, 0.05], 0.05, 0.1, ec='#762a83', fc='#c2a5cf')
	end3Rect = mpl.patches.Rectangle([0.025, 0.05], 0.05, 0.1, ec='#1b7837', fc='#a6dba0')
	ax1.legend([start5Rect, end5Rect, start3Rect, end3Rect], ['5p start', '5p end', '3p start', '3p end'])
	#plt.savefig(samp+'_startbp_nofliers.png')
	#plt.clf()

	# Mature sequence start bp groups - with fliers
	fig2 = plt.figure(figsize=(15, 10))
	ax3 = fig2.add_subplot(111)

	bp_start5 = ax3.boxplot(start5p1, positions=ind1-2*width, patch_artist=True, widths=width, sym='o')
	bp_end5 = ax3.boxplot(end5p1, positions=ind1-width, patch_artist=True, widths=width, sym='o')
	bp_start3 = ax3.boxplot(start3p1, positions=ind1, patch_artist=True, widths=width, sym='o')
	bp_end3 = ax3.boxplot(end3p1, positions=ind1+width, patch_artist=True, widths=width, sym='o')

	
	for box in bp_start5['boxes']:
		box.set(color='#2166ac', facecolor='#92c5de')
	for box in bp_end5['boxes']:
		box.set(facecolor='#f4a582', color='#b2182b')
	for box in bp_start3['boxes']:
		box.set(color='#762a83', facecolor='#c2a5cf')
	for box in bp_end3['boxes']:
		box.set(facecolor='#a6dba0', color='#1b7837')

	for whisk in bp_start5['whiskers']:
		whisk.set(color='#2166ac', linewidth=2)
	for whisk in bp_end5['whiskers']:
		whisk.set(color='#b2182b', linewidth=2)
	for whisk in bp_start3['whiskers']:
		whisk.set(color='#762a83', linewidth=2)
	for whisk in bp_end3['whiskers']:
		whisk.set(color='#1b7837', linewidth=2)


	for median in bp_start5['medians']:
		median.set(color='#7a0177', linewidth=2)
	for median in bp_end5['medians']:
		median.set(color='#7a0177', linewidth=2)
	for median in bp_start3['medians']:
		median.set(color='#7a0177', linewidth=2)
	for median in bp_end3['medians']:
		median.set(color='#7a0177', linewidth=2)

	for cap in bp_start5['caps']:
		cap.set(color='#2166ac', linewidth=2)
	for cap in bp_end5['caps']:
		cap.set(color='#b2182b', linewidth=2)
	for cap in bp_start3['caps']:
		cap.set(color='#762a83', linewidth=2)
	for cap in bp_end3['caps']:
		cap.set(color='#1b7837', linewidth=2)

	for flier in bp_start5['fliers']:
		flier.set(alpha=0.6, color='#2166ac')
	for flier in bp_end5['fliers']:
		flier.set(alpha=0.6, color='#b2182b')
	for flier in bp_start3['fliers']:
		flier.set(alpha=0.6, color='#762a83')
	for flier in bp_end3['fliers']:
		flier.set(alpha=0.6, color='#1b7837')


	ax3.set_xticks(ind1, minor=False)
	ax3.set_xticklabels(names1, minor=False, fontdict=False, rotation=20,fontsize=20, ha='right')
	ax3.tick_params(axis='y', labelsize=20)
	ax3.set_ylabel('log$_2$(#shortreads/#mature sequences)',fontsize=20)
	ax3.spines['top'].set_visible(False)
	ax3.spines['right'].set_visible(False)
	ax3.xaxis.set_ticks_position('bottom')
	ax3.yaxis.set_ticks_position('left')
	ax3.legend([start5Rect, end5Rect, start3Rect, end3Rect], ['5p start', '5p end', '3p start', '3p end'])

	fig1.suptitle('Ago1-Ago3 shortread association by 5\' end basepairing', fontsize=30)
	fig1.subplots_adjust(bottom=0.15)
	fig1.savefig(samp+'_BPgroups.png')


	fig2.suptitle('Ago1-Ago3 shortread association by 5\' end basepairing', fontsize=30)
	fig2.subplots_adjust(bottom=0.15)
	fig2.savefig(samp+'_BPgroups_fliers.png')

	plt.clf()


def createBPData(shortreads):
	"""
	Creates the list of data needed to produce the plots.
	"""
	uselog = True # Defines if #SR/#mat should be log2'ed or not
	start5p, start3p, end5p, end3p = list(), list(), list(), list()
	
	for shortie in shortreads:
		print 'What is shortie?'
		print type(shortie)
		print len(shortie)
		shortread = shortie[0]
		expr = shortie[1]
		SRcount = shortread.count
		div = (SRcount/expr)
		x = math.log(div, 2) if uselog else div

		pos = shortread.position
		prime = shortread.prime

		if pos=='start' and prime==5:
			start5p.append(x)
		elif pos=='start' and prime==3:
			start3p.append(x)
		elif pos=='end' and prime==5:
			end5p.append(x)
		elif pos=='end' and prime==3:
			end3p.append(x)

	return start5p, start3p, end5p, end3p

def findGroupsTotal(mature_lists, hairpin_lists, shortread_lists, names):
	p5SR, p3SR, p53SR, pSR, sr0, sr1, sr2, sr3, sr4, sr5  = list(), list(), list(), list(), list(), list(), list(), list(), list(), list()

	for i in range(len(mature_lists)):
		p5SR_new, p3SR_new, p53SR_new, pSR_new, sr0_new, sr1_new, sr2_new, sr3_new, sr4_new, sr5_new = findGroups(mature_lists[i], hairpin_lists[i], shortread_lists[i], names[i])
		p5SR.extend(p5SR_new)
		p3SR.extend(p3SR_new)
		p53SR.extend(p53SR_new)
		pSR.extend(pSR_new)
		sr0.extend(sr0_new)
		sr1.extend(sr1_new)
		sr2.extend(sr2_new)
		sr3.extend(sr3_new)
		sr4.extend(sr4_new)
		sr5.extend(sr5_new)
	return p5SR, p3SR, p53SR, pSR, sr0, sr1, sr2, sr3, sr4, sr5


def findGroups(matures, hairpins, shortreads, name):
	p5matbp, p3matbp, p3and5matbp, nomatbp, mat0bp, mat1bp, mat2bp, mat3bp, mat4bp, mat5bp =  dict(), dict(), dict(), dict(), dict(), dict(), dict(), dict(), dict(), dict()
	p5SR, p3SR, p53SR, pSR, sr0, sr1, sr2, sr3, sr4, sr5  = list(), list(), list(), list(), list(), list(), list(), list(), list(), list()
	
	noPair = list()
	print 'number of matures for %s is %d' % (name,len(matures))

	# Find mature sequence pairs and find their bp combination
	for ref in matures:
		if len(matures[ref])<2:
			noPair.append(ref)
			#print 'There is no pair for ref %s' % ref
			continue
		p3, p5 = -1.0,-1.0
		mat3, mat5 = None, None
		for mature in matures[ref]:
			if mature.prime == 3 and mature.count > p3:
				mat3 = mature
				p3 = mature.count
			elif mature.prime==5 and mature.count > p5:
				mat5 = mature
				p5 = mature.count
		if mat3 == None or mat5 == None: 
			if mat5 != None: noPair.append(mat5.hairpin_id)
			if mat3 != None: noPair.append(mat3.hairpin_id)
			continue #Todo: handle these?


		#Find the bp combination
		if isPaired(hairpins[ref], mat3.offset) and isPaired(hairpins[ref], mat5.offset):
			p3and5matbp[mat5.miRNA_id] = mat5.count
			p3and5matbp[mat3.miRNA_id] = mat3.count
		elif isPaired(hairpins[ref], mat3.offset):
			p3matbp[mat5.miRNA_id] = mat5.count
			p3matbp[mat3.miRNA_id] = mat3.count
		elif isPaired(hairpins[ref], mat5.offset):
			p5matbp[mat5.miRNA_id] = mat5.count
			p5matbp[mat3.miRNA_id] = mat3.count
		else:
			nomatbp[mat5.miRNA_id] = mat5.count
			nomatbp[mat3.miRNA_id] = mat3.count



	#Find mature sequences that have 0-5 paired nt's in index range 8-12 for 5p mature sequence
	for ref in matures:
		for mature in matures[ref]:
			if ref in noPair: continue
			count = 0
			start = mature.offset+8
			end = mature.offset+12
			for i in range(start, end+1):
				if isPaired(hairpins[ref], i): count += 1 
			if count==0:
				mat0bp[mature.miRNA_id] = mature.count
			elif count==1:
				mat1bp[mature.miRNA_id] = mature.count
			elif count==2:
				mat2bp[mature.miRNA_id] = mature.count
			elif count==3:
				mat3bp[mature.miRNA_id] = mature.count
			elif count==4:
				mat4bp[mature.miRNA_id] = mature.count
			elif count==5:
				mat5bp[mature.miRNA_id] = mature.count
			else:
				if mature.hairpin_id not in noPair:
					print('Something went wrong, we have a mature seq with count %d: %s' % (count, mature.miRNA_id))


	for ref in shortreads:
		for shortie in shortreads[ref]:
			if shortie.mature_id in p3and5matbp:
				p53SR.append((shortie, p3and5matbp[shortie.mature_id]))
			elif shortie.mature_id in p5matbp:
				p5SR.append((shortie, p5matbp[shortie.mature_id]))
			elif shortie.mature_id in p3matbp:
				p3SR.append((shortie, p3matbp[shortie.mature_id]))
			elif shortie.mature_id in nomatbp:
				pSR.append((shortie, nomatbp[shortie.mature_id]))
			else:
				if shortie.hairpin_id not in noPair:
					print 'Found a shortread with ungrouped mature id: %s, of a count %g' % (shortie.mature_id, shortie.count)
			if shortie.mature_id in mat0bp:
				sr0.append((shortie, mat0bp[shortie.mature_id]))
			elif shortie.mature_id in mat1bp:
				sr1.append((shortie, mat1bp[shortie.mature_id]))
			elif shortie.mature_id in mat2bp:
				sr2.append((shortie, mat2bp[shortie.mature_id]))
			elif shortie.mature_id in mat3bp:
				sr3.append((shortie, mat3bp[shortie.mature_id]))
			elif shortie.mature_id in mat4bp:
				sr4.append((shortie, mat4bp[shortie.mature_id]))
			elif shortie.mature_id in mat5bp:
				sr5.append((shortie, mat5bp[shortie.mature_id]))
			else:
				if shortie.hairpin_id not in noPair:
					print 'We have a problem, this SR\'s mature id is not in any list: %s' % shortie.mature_id
	

	return p5SR, p3SR, p53SR, pSR, sr0, sr1, sr2, sr3, sr4, sr5

def isPaired(hairpin, index):
	"""
	Return true if nt is paired or not
	"""
	return  False if hairpin.foldstructure[index]=='.' else True  


if __name__ == '__main__':
	main()