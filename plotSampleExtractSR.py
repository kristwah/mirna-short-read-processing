import sys
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import math
import shortreadProcesser as sp


def main():
	ago1_hp, ago1_mat, ago1_sr, ago1_nosr = sp.produceShortreadsMaturesandNoshortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797059_alignments.txt', 'SRR797059')
	ago2_hp, ago2_mat, ago2_sr, ago2_nosr = sp.produceShortreadsMaturesandNoshortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797060_alignments.txt', 'SRR797060')
	ago3_hp, ago3_mat, ago3_sr, ago3_nosr = sp.produceShortreadsMaturesandNoshortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797061_alignments.txt', 'SRR797061')
	ago4_hp, ago4_mat, ago4_sr, ago4_nosr = sp.produceShortreadsMaturesandNoshortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797062_alignments.txt', 'SRR797062')
	ctrl_hp, ctrl_mat, ctrl_sr, ctrl_nosr = sp.produceShortreadsMaturesandNoshortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797063_alignments.txt', 'SRR797063')

	refsSR = findSRRefs(ago1_sr, ago2_sr, ago3_sr, ago4_sr, ctrl_sr)
	refsNoSR = findNoSRRefs(ago1_nosr, ago2_nosr, ago3_nosr, ago4_nosr, ctrl_nosr)
	dataSR, dataNoSR = createData(refsSR, refsNoSR, [ago1_sr, ago2_sr, ago3_sr, ago4_sr, ctrl_sr], [ago1_mat, ago2_mat, ago3_mat, ago4_mat, ctrl_mat])
	
	plotTotalResult(dataSR, 'GSE45506 Shortreads vs mature sequences', 'GSE45506_SRvsMat_threshold')
	plotTotalResult(dataNoSR, 'GSE45506 Shortreads vs mature sequences', 'GSE45506_noSRvsMat_threshold')

	findClassification(refsSR, refsNoSR)


def findClassification(refsSR, refsNoSR):
	
	both = dict()

	for ref in refsNoSR:
		if ref in refsSR:
			if ref not in both:
				both[ref] = 0
		
	for ref in both:
		del refsSR[ref]
		del refsNoSR[ref]

	sizes = [len(refsSR), len(refsNoSR), len(both)]
	labels = ['Shortreads','No shortreads' , 'Combination of \nshortreads and \nno shortreads']
	colors = ['#8dd3c7', '#ffffb3', '#bebada']
	explode = [0.1,0.1,0.2]
	plt.axis("equal")
	plt.pie(sizes, explode=explode, labels=labels, colors=colors, autopct='%1.1f%%')
	plt.title('GSE45506 Shortread classification')
	plt.savefig('GSE45506_piechart.png')
	plt.clf()



def createData(refsSR, refsNoSR, shortreads, matures):
	dataSR, dataNoSR = list(), list()

	for i in range(len(shortreads)):
		dataSR.append(createSRData(refsSR, shortreads[i], matures[i]))
		dataNoSR.append(createSRData(refsNoSR, shortreads[i], matures[i]))

	return dataSR, dataNoSR

def createSRData(refs, shorts, matures):
	data = list()
	for ref in refs:
		SRcount = 0
		try:
			for shortie in shorts[ref]:
				SRcount += shortie.count
		except:
			pass
		matCount = 0
		for mat in matures[ref]:
			matCount += mat.count

		data.append((SRcount, matCount))
	return data


def findRefPrSample(shortreads):
	"""
	Find all references with a shortread count above 100 
	"""
	refs = dict()
	for ref in shortreads:
		count = 0
		for shortread in shortreads[ref]:
			count += shortread.count
		if count>1:
			refs[ref] = count
	return refs

def findNoRefsPrSample(noShortreads):
	refs = dict()
	for ref in noShortreads:
		if noShortreads[ref] > 1:
			refs[ref] = 0
	return refs

def findNoSRRefs(ago1_nosr, ago2_nosr, ago3_nosr, ago4_nosr, ctrl_nosr):
	refs1 = findNoRefsPrSample(ago1_nosr)
	refs2 = findNoRefsPrSample(ago2_nosr)
	refs3 = findNoRefsPrSample(ago3_nosr)
	refs4 = findNoRefsPrSample(ago4_nosr)
	refs5 = findNoRefsPrSample(ctrl_nosr)
	candidates = [refs1, refs2, refs3, refs4, refs5]

	print 'found no refs'
	print len(refs1)
	print len(refs2)
	print len(refs3)
	print len(refs4)
	print len(refs5)

	noSRrefs = dict()

	for refs in candidates:
		for ref in refs:
			if ref not in noSRrefs:
				noSRrefs[ref] = None
	return noSRrefs


def findSRRefs(ago1_sr, ago2_sr,ago3_sr, ago4_sr, ctrl_sr):
	refs1 = findRefPrSample(ago1_sr)
	refs2 = findRefPrSample(ago2_sr)
	refs3 = findRefPrSample(ago3_sr)
	refs4 = findRefPrSample(ago4_sr)
	refs5 = findRefPrSample(ctrl_sr)
	candidates = [refs1, refs2, refs3, refs4, refs5]

	print 'found refs'
	print len(refs1)
	print len(refs2)
	print len(refs3)
	print len(refs4)
	print len(refs5)

	refs = dict()
	for refss in candidates:
		for ref in refss:
			hits = [1 for cand in candidates if cand.has_key(ref)]
			if len(hits)>2:
				if ref not in refs:
					refs[ref] = None
	print 'Jes, have found SR refs, total: %d ' % len(refs)
	return refs



def plotTotalResult(data, title, name):
	"""
	Plots a boxplot with the total result for each sample.
	"""
	names = ['Ago1 IP', 'Ago2 IP', 'Ago3 IP', 'Ago4 IP', 'Control IP']
	totals = list()

	j=1
	for sample in data:
		SRcount, matCount = 0,0
		for item in sample:
			SRcount += item[0]
			matCount += item[1]

		SRcount += 1
		matCount += 1
		SR_count = math.log(SRcount, 2)
		mat_Count = math.log(matCount, 2)

		print 'Sample no %d has a srcount of %f and mat count of %f' % (j, SR_count, mat_Count)
		totals.append((SR_count, mat_Count))
		j+=1
	
	y,x = zip(*totals)

	ax = plt.subplot(111)
	ax.scatter(x, y, alpha=0.5, s=190, c='#4292c6')

	for i, txt in enumerate(names):
		ax.annotate(txt, (x[i]+0.05, y[i]+0.05))
	
	plt.title(title)
	plt.ylabel('log$_2$(normalized short read counts)')
	plt.xlabel('log$_2$(normalized full length counts)')
	plt.savefig(name+'.png')
	plt.clf()


if __name__ == '__main__':
	main()




