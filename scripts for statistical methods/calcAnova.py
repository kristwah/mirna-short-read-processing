import shortreadProcesser as sp

def main():
	"""
	This script retrieves short reads and prints them in a table format to be used in r for calculating anovas.
	See anova-r.script for details on further processing.
	"""
	Ago1 = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797059_alignments.txt', 'SRR797059')
	Ago2 = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797060_alignments.txt', 'SRR797060')
	Ago3 = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797061_alignments.txt', 'SRR797061')

	printData([Ago1, Ago2, Ago3])

def printData(agos):
	"""
	Method for printing a table file to use in anova-R script
	"""
	t = open('shortread_table.txt', 'w')
	newLine = ['ago', 'strand', 'position', 'offset', 'count']
	t.write('\t'.join(newLine))
	t.write('\n')

	for i,ago in enumerate(agos):
		name = 'ago'+str(i+1)
		for ref in ago:
			for Shortread in ago[ref]:
				if abs(Shortread.mat_offset)>2: continue
				newLine = [name, str(Shortread.prime), Shortread.position, str(Shortread.mat_offset), str(Shortread.count)]
				t.write('\t'.join(newLine))
				t.write('\n')
	t.close()


if __name__ == '__main__':
	main()