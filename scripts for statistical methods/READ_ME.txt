Statistical methods for sample-specific calculations and across multiple samples.

Sample-specific methods:
	Run ttest_script.py

Across multiple samples:
	Run calcAnova.py to generate input data to the calculation
	Open R in command line mode, and rund commands given in anova-r_commands.txt