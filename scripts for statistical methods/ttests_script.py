from scipy import stats
import math
import shortreadProcesser as sp

def main():
    ago1_sr = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797059_alignments.txt', 'SRR797059')
    ago2_sr = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797060_alignments.txt', 'SRR797060')
    ago3_sr = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797061_alignments.txt', 'SRR797061')
    ago4_sr = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797062_alignments.txt', 'SRR797062')
    ctrl_sr = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797063_alignments.txt', 'SRR797063')

    analyzeBoxplot(ago1_sr, 'Ago1')
    analyzeBoxplot(ago2_sr, 'Ago2')
    analyzeBoxplot(ago3_sr, 'Ago3')
    analyzeBoxplot(ago4_sr, 'Ago4')
    analyzeBoxplot(ctrl_sr, 'Control IP')

def analyzeBoxplot(shortread_map, name):
	stat3 = createData(shortread_map, 3)
	stat5 = createData(shortread_map, 5)
	
	start3 = stat3[0][0]
	end3 = stat3[0][1]
	start5 = stat5[0][0]
	end5 = stat5[0][1]
	
    print 'Generating t-tests for %s' % name
    
	#Perform t-test for independent samples, assuming equal population variance
	t, p = stats.ttest_ind(start5, start3)
	print 'T-test for startoffset pos 0 show t = %g and p = %g' % (t, p)
	t, p = stats.ttest_ind(end5, end3)
	print 'T-test for endoffset pos 0 show t = %g and p = %g' % (t, p)
	
	#Perform welch's t-test
	t, p = stats.ttest_ind(start5, start3, equal_var=False)
	print 'Welch\'s t-test for startoffset pos 0 show t = %g and p = %g' % (t, p)
	t, p = stats.ttest_ind(end5, end3, equal_var=False)
	print 'Welch\'s t-test for endoffset pos 0 show t = %g and p = %g' % (t, p)
	
	#Perform Wilcox's test
	t, p = stats.ranksums(start5, start3)
	print 'Wilcox\'s test for startoffset pos 0 show t = %g and p = %g' % (t, p)
	t, p = stats.ranksums(end5, end3)
	print 'Wilcox\'s test for endoffset pos 0 show t = %g and p = %g' % (t, p)

    print 'Finished with %s \n' % name
	
def createData(shortread_map, prime):
	stat = {}
	labels, start, end = [], [], []

	for ref in shortread_map:
		for Shortread in shortread_map[ref]:
			if Shortread.prime==prime:
				# If variation stability is needed, use logarithm of counts with base 2
				count = math.log(Shortread.count, 2)
				if Shortread.position=='start':
					offset = Shortread.mat_offset
					if abs(offset)>10:
						offset='other'
					if offset in stat:
						stat[offset][0].append(count)
					else:
						stat[offset]=[[count], []]
				elif Shortread.position=='end':
					offset = Shortread.mat_offset
					if abs(offset)>10:
						offset = 'other'
					if offset in stat:
						stat[offset][1].append(count)
					else:
						stat[offset]=[[],[count]]

	return stat

if __name__ == '__main__':
    main()