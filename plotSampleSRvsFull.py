import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import math
import shortreadProcesser as sp

def main():

	ago1_hp, ago1_mat, ago1_sr = sp.produceShortreadsAndMatures('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797059_alignments.txt', 'SRR797059')
	ago2_hp, ago2_mat, ago2_sr = sp.produceShortreadsAndMatures('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797060_alignments.txt', 'SRR797060')
	ago3_hp, ago3_mat, ago3_sr = sp.produceShortreadsAndMatures('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797061_alignments.txt', 'SRR797061')
	ago4_hp, ago4_mat, ago4_sr = sp.produceShortreadsAndMatures('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797062_alignments.txt', 'SRR797062')
	ctrl_hp, ctrl_mat, ctrl_sr = sp.produceShortreadsAndMatures('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797063_alignments.txt', 'SRR797063')
	
	tot2 = findTotData([ago1_sr, ago2_sr, ago3_sr, ago4_sr, ctrl_sr], [ago1_mat, ago2_mat, ago3_mat, ago4_mat, ctrl_mat])
	ago1 = findEndData(ago1_sr, ago1_mat)
	ago2 = findEndData(ago2_sr, ago2_mat)
	ago3 = findEndData(ago3_sr, ago3_mat)
	ago4 = findEndData(ago4_sr, ago4_mat)


	#plot(tot2, 'GSE shortreads vs. mature sequences for highest strand', 'log$_2$ shortread counts', 'GSE45506_SRvsMat_highest')
	plot(ago1, 'Ago1 short reads vs. mature sequences', 'log$_2$ shortread counts', 'Ago1_SRvsMat')
	plot(ago2, 'Ago2 short reads vs. mature sequences', 'log$_2$ shortread counts', 'Ago2_SRvsMat')
	plot(ago3, 'Ago3 short reads vs. mature sequences', 'log$_2$ shortread counts', 'Ago3_SRvsMat')
	plot(ago4, 'Ago4 short reads vs. mature sequences', 'log$_2$ shortread counts', 'Ago4_SRvsMat')


	#plot(a1ends, 'Ago1 end-reads vs. mature sequences', 'log$_2$ end-read counts', 'Ago1_EndvsMat')


	#plot(totals, 'GSE45506 shortreads vs. mature sequences', 'log$_2$ shortread counts', 'GSE45506_SRvsMat')
	#plot(most, 'GSE45506 SR vs Mature for the highest prime', 'log$_2$ shortread counts', 'GSE45506_SRvsMat_highest')
	#plot(least, 'GSE45506 SR vs Mature for the lowest prime', 'log$_2$ shortread counts', 'GSE45506_SRvsMat_lowest')
	#plot(starts, 'GSE45506 start-reads vs mature sequences', 'log$_2$ start-read counts', 'GSE45506_StartvsMat')
	#plot(ends, 'GSE45506 end-reads vs mature sequences', 'log$_2$ end-read counts', 'GSE45506_EndvsMat')

def plot(data, title, label, name):
	"""
	The data are touples of (read_count, mature_count). We want read_count on y-axis and mature_count on x-axis.
	"""
	y, x = zip(*data)

	plt.figure()
	plt.scatter(x, y, alpha=0.3, s=70, c='#4292c6')
	plt.title(title)
	plt.ylabel(label)
	plt.xlabel('log$_2$ mature sequence counts')
	plt.savefig(name+'.png')
	plt.clf()



def findTotData(shortreads, matures):
	data = list()
	for i in range(len(shortreads)):
		newData = findEndData(shortreads[i], matures[i])
		data.extend(newData)
	return data


def findEndData(shortreads, miRNAs_mature):
	ends = dict()
	endsHighest = list()
	noEnds = list()
	noExpr = list()
	notHighest = list()
	onLowestStrand = list()

	for ref in shortreads:
		for Shortread in shortreads[ref]:
			#if Shortread.position=='start': continue
			#if not Shortread.highest_prime: continue
			mature = Shortread.mature_id
			count = Shortread.count
			
			if ends.has_key(mature):
				ends[mature] += count
			else:
				ends[mature] = count



	data = list()
	for ref in miRNAs_mature:
		for mat in miRNAs_mature[ref]:
			expr = mat.count +1
			if not ends.has_key(mat.miRNA_id): 
				noEnds.append(mat.miRNA_id)
				if not mat.expressed:
					noExpr.append(mat.miRNA_id)
				elif len(miRNAs_mature[ref])>2:
					lower = False
					for mat2 in miRNAs_mature[ref]:
						if mat2.prime == mat.prime and mat2.expressed:
							lower = True
					if lower: notHighest.append(mat.miRNA_id)
					if not mat.highest_prime: onLowestStrand.append(mat.miRNA_id)
				continue
			if mat.highest_prime:
				endsHighest.append(mat.miRNA_id)

			expr2 = math.log(expr, 2)
			end = ends[mat.miRNA_id] +1.0
			end2 = math.log(end, 2)
			data.append((end2, expr2))
	print 'Number of mats with shorties is %d, and of these %d relies on the highest strand;  and candidate seqs with no shorties for some reason is: %d, where %d is on the lowest expressed strand' % (len(ends), len(endsHighest),(len(noEnds) - (len(noExpr) + len(notHighest))), len(onLowestStrand))
	return data
				
def findMatureCount(ref_id, mat_id, matures):
	for mat in matures[ref_id]:
		if mat.miRNA_id == mat_id:
			return mat.count 
	return 0.0


if __name__ == '__main__':
	"""
	Script executed in command mode
	"""
	main()