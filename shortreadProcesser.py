import sys
import string
import math
import os
from Bio import SeqIO
import plotStatistics as ps
import plotReferences as pr
import plotEndvsFull as pe
#import plotDifferentSRs as prs
import plotAlignmentsBoxplot as pab
#import bpAnalysis as ba

class Shortread:
	def __init__(self, hairpin_id, offset, sequence, count, mature_id, position, prime, mat_offset, highest_prime):
		self.hairpin_id = hairpin_id
		self.offset = offset
		self.sequence = sequence
		self.count = count
		self.mature_id = mature_id
		self.position = position
		self.prime = prime
		self.mat_offset = mat_offset
		self.highest_prime = highest_prime

class MicroRNA:
	def __init__(self, hairpin_id, miRNA_id, count, offset, sequence, prime):
		self.hairpin_id = hairpin_id
		self.miRNA_id = miRNA_id
		self.count = count
		self.offset = offset
		self.sequence = sequence
		self.prime = prime
		self.expressed = True
		self.highest_prime = None

	def setUnexpressed(self):
		self.expressed = False

	def setHighestPrime(self, highest_prime):
		if self.prime==highest_prime:
			self.highest_prime = True
		else:
			self.highest_prime = False


class Hairpin:
	def __init__(self, hairpin_id, sequence, foldstructure):
		self.hairpin_id = hairpin_id
		self.sequence = sequence
		self.foldstructure = foldstructure


def produceShortreadsAndMatures(refFile, dotBracketFile, matFile, sampFile, name):
	"""
	Run when called from another script needing both shortreads and matures
	"""
	global hairpin_map, miRNA_unique, miRNA_total, miRNAs_highexpr, shortreads, mors, shortread_map, bucketSize, microRNA_map

	hairpin_map = dict()
	microRNA_map = dict()
	miRNA_unique  = dict()
	miRNA_total = dict()
	miRNAs_highexpr = dict()
	shortreads = dict()
	mors = dict()
	shortread_map = dict()
	bucketSize = 5

	readHairpins(refFile, dotBracketFile)
	readMatures(matFile)
	readSample(sampFile)
	alignShortreads()
	removeUnexpressed(name)

	return hairpin_map, microRNA_map, shortread_map


def produceShortreadsMaturesandNoshortreads(refFile, dotBracketFile, matFile, sampFile, name):
	global hairpin_map, miRNA_unique, miRNA_total, miRNAs_highexpr, shortreads, mors, shortread_map, bucketSize, microRNA_map

	hairpin_map = dict()
	microRNA_map = dict()
	miRNA_unique  = dict()
	miRNA_total = dict()
	miRNAs_highexpr = dict()
	shortreads = dict()
	mors = dict()
	shortread_map = dict()
	bucketSize = 5

	readHairpins(refFile, dotBracketFile)
	readMatures(matFile)
	readSample(sampFile)
	alignShortreads()
	removeUnexpressed(name)
	noReads = findNoReads()

	return hairpin_map, microRNA_map, shortread_map, noReads

def produceShortreads(refFile, dotBracketFile, matFile, sampFile, name):
	"""
	Run when called from another script
	"""
	global hairpin_map, miRNA_unique, miRNA_total, miRNAs_highexpr, shortreads, mors, shortread_map, bucketSize, microRNA_map

	hairpin_map = dict()
	microRNA_map = dict()
	miRNA_unique  = dict()
	miRNA_total = dict()
	miRNAs_highexpr = dict()
	shortreads = dict()
	mors = dict()
	shortread_map = dict()
	bucketSize = 5

	readHairpins(refFile, dotBracketFile)
	readMatures(matFile)
	readSample(sampFile)
	alignShortreads()
	removeUnexpressed(name)

	return shortread_map
		
def readMatures(filename):
	"""
	Reads input file of mature sequences for reference ID's.
	Format of input lines:
	mmu-let-7g	6	TGAGGTAGTAGTTTGTACAGTT	
	"""
	f = open(filename, 'r')
	seq, ref = '',''
	global microRNA_map
	
	for line in f:
		skip = False
		data = line.split()
		ref = data[0]
		offset = int(data[1])
		sequence = data[2]
		if isInvalidSample(ref, offset, sequence): continue
		if ref in hairpin_map:
			#There may be multiple mature seq's pr reference ID, so add a postfix number to mature ID's showing the total count.
			count = len(microRNA_map[ref])
			if count>0:
				for miRNA in microRNA_map[ref]:
					if miRNA.sequence==sequence and miRNA.offset==offset:
						skip = True		
			if not skip:
				newMat = ref + str(count+1)
				newMiRNA = MicroRNA(ref, newMat, 0.0, offset, sequence, findPrime(ref, offset))
				microRNA_map[ref].append(newMiRNA)
	f.close()
	
def readHairpins(hairpinfile, foldfile):
	"""
	Load hairpin file using biopython
	"""
	global hairpin_map
	f = open(hairpinfile)
	folds = readFoldstructures(foldfile)
	for record in SeqIO.parse(f, 'fasta'):
		try:
			hairpin_map[record.id] = Hairpin(record.id, record.seq, folds[record.id])
			addRefAsKey(record.id, record.seq)
		except KeyError:
			print('Missing fold structure  for: ' + record.id)
			continue

def readFoldstructures(foldfile):
	folds = {}
	f = open(foldfile)
	ref, seq = '', ''
	inProcess = False
	
	for line in f:
		line = line.strip('\r\n') #If file is made in Windows OS
		line = line.strip('\n') 
		if line[0]=='>' and inProcess:
			folds[ref] = seq
			ref = line[1:]
			seq = ''
		elif line[0]=='>':
			ref = line[1:]
			folds[ref] = ''
			inProcess = True
		else:
			seq += line
	folds[ref] = seq
	f.close()
	return folds

def readRNAs (filename):
	"""
	Reads reference sequences and their ID's.
	Initiates entries in all relevant dictionaries with the reference ID's.
	Format of input lines:
	>mmu-let-7g MI0000137 Mus musculus let-7g stem-loop
	CCAGGCTGAGGTAGTAGTTTGTACAGTTTGAGGGTCTATGATACCACCCGGTACAGGAGA
	TAACTGTACAGGCCACTGCCTTGCCAGG
	"""
	global hairpin_map, miRNA_total
	
	f = open(filename, 'r')
	seq, ref = "", ""
	skipline = False
	for line in f:
		line = line.strip('\r\n') #For files created in Windows
		line = line.strip('\n')

		if line == '': 
			continue
		elif line[0]=='>':
			if isSeq(seq) and not skipline:
				addRefAsKey(ref, seq)
				seq = ''
				ref = ''
			elif seq != '' and ref in hairpin_map:
				del hairpin_map[ref]
			ref = line[1:].split()[0]
			if not isRef(ref):
				skipline = True
				continue
			hairpin_map[ref] = None
			skipline = False
		elif skipline:
			continue
		elif seq != "" and skipline is False:
			seq = ''.join([seq, line])
		elif skipline is False:
			seq = line
	if skipline is False:
		if isSeq(seq):
			addRefAsKey(ref, seq)
		elif seq != '' and ref in hairpin_map:
			del hairpin_map[ref]
	f.close()
	
def addRefAsKey(ref, seq):
	"""
	Adds a new reference ID and it's sequence to all relevant dictionaries.
	"""
	global  miRNA_unique, shortreads
	if ref =='hsa-mir-1234':
		seq = 'GUGAGUGUGGGGUGGCUGGGGCGGGGGGGGCCCGGGGACGGCUUGGGCCUGCCUAGUCGGCCUGACCACCCACCCCACAG'

	miRNA_unique[ref] = countDict(seq)
	miRNA_total[ref] = countDict(seq)
	miRNAs_highexpr[ref] = dict()
	shortreads[ref] = list()
	mors[ref] = list()
	shortread_map[ref] = list()
	microRNA_map[ref] = list()
	
def isRef(ref):
	"""
	Help method for checking if new reference ID is valid.
	Choose a ID limit for references to be included.
	"""
	if ref.isalnum(): return False
	s = ref.split("-")
	if (not s[0].isalpha()) or (not s[1].isalpha()) or (not s[2].isalnum()): return False
	if s[len(s)-1].isalpha(): return False
	
	return False if s[2].isdigit() and int(s[2])>5000 else True
	
def isSeq(seq):
	"""
	Help method for checking if new sequence is valid.
	"""
	allowed = 'ACGT'
	if seq == '' : return False
	for s in seq.upper():
		if s not in allowed:
			return False
	return True

def isInvalidSample(ref, offset, seq):
	"""
	Help method for checking if new sample and reference ID is valid.
	"""
	if ref not in hairpin_map:
		return True
	elif not isSeq(seq):
		return True
	elif len(hairpin_map[ref].sequence)-offset < len(seq):
		return True
	else:
		return False
	
def countDict (string):
	"""
	Creates dictionaries for indexes and counts of a given sequence's length.
	"""
	l = len(string)
	j=0
	if l%bucketSize > 0: j=1
	counts = dict()
	for i in range (l//bucketSize + j - 1):
		counts[i]=[0]*l	
	return counts
	
def readSample(filename):
	"""
	Reads sample data from given file.
	Updates counts for relevant index ranges in existing reference sequences.
	Format of input lines:
	4-86385	mmu-mir-10b	4	TACCCTGTAGAACCGAATTTGT	0
	"""
	global miRNA_total, miRNA_unique, shortreads
	totCount = 0.0
	f = open(filename, 'r')
	ref, seq, offset, count = "", "", 0, 0.0
	
	for line in f:
		sample = line.split()
		count = int(sample[0].split('-')[1])
		totCount += count
	f.close()
	
	f = open(filename, 'r')
	for line in f:
		sample = line.split()
		count = int(sample[0].split('-')[1])
		nCount = count/(float(sample[4])+1)
		rpm = (nCount / totCount) * 1000000.0
		
		ref = sample[1]
		offset = int(sample[2])
		seq = sample[3]
		if isInvalidSample(ref, offset, seq): 
			ref, seq, offset, count = '', '', 0, 0.0
			continue
		
		bucketIndex = (len(seq)-1) // bucketSize
		success = True 
		for i in range(offset, offset+len(seq)):
			try:
				miRNA_unique[ref][bucketIndex][i] += 1
				miRNA_total[ref][bucketIndex][i] += rpm
			except:
				success = False
				continue
		
		#If bucketindex is 2 we have a short read
		if bucketIndex==2 and success: 
			shortreads[ref].append([offset, seq, rpm, ''])

		#addShortRead(ref, offset, seq, count/(float(sample[4])+1))
		
		#Save count if sequence is a mature seq
		if success and bucketIndex in range(3,5):
			for miRNA in microRNA_map[ref]:
				if miRNA.offset==offset and miRNA.sequence==seq:
					miRNA.count += rpm
		
		#Add read in case it is higher expressed than any mature sequence for this reference hairpin
		if success: addRead(ref, seq, offset, rpm)
	
	#Check if any read was higher expressed than the mature sequences for this reference hairpin
	for ref in hairpin_map:
		checkForNewMatureSeq(ref)
	f.close()

def addRead(ref, seq, offset, count):
	"""
	Adds new reads with higher expression in sample data than already processed.
	"""
	global miRNAs_highexpr
	prime = findPrime(ref, offset)
	
	if (len(seq)-1)//bucketSize not in range(3,5): 
		return
	if miRNAs_highexpr[ref].has_key(prime):
		if count > miRNAs_highexpr[ref][prime][0]:
			miRNAs_highexpr[ref][prime] = [count, offset, seq]
	else:
		miRNAs_highexpr[ref][prime] = [count, offset, seq]
		
def checkForNewMatureSeq(ref):
	"""
	Checks if the highest expressed sequence for a given reference ID is a mature sequence.
	If not, this sequence is added to existing mature sequences with a postfix 'N' to the mature ID.
	"""
	global miRNAs_highexpr, microRNA_map, MicroRNA
	mats5, mats3 = [], []
	
	for miRNA in microRNA_map[ref]:
		matPrime = findPrime(ref, miRNA.offset)
		if matPrime==3: 
			mats3.append(miRNA.sequence)
		elif matPrime==5: 
			mats5.append(miRNA.sequence)
	
	if miRNAs_highexpr[ref].has_key(3):
		if miRNAs_highexpr[ref][3][2] not in mats3 and len(miRNAs_highexpr[ref][3][2])>18:
			newSeq = miRNAs_highexpr[ref][3][2]
			newOffset = miRNAs_highexpr[ref][3][1]
			newCount = miRNAs_highexpr[ref][3][0]
			newID = ref+'3N'
			miRNAs_highexpr[ref][3].append(newID)
			newMiRNA = MicroRNA(ref, newID, newCount, newOffset, newSeq, findPrime(ref, newOffset))
			microRNA_map[ref].append(newMiRNA)  #Adds 'N' postfix to denote this is a new mature sequence
		else:
			id = [miRNA.miRNA_id for miRNA in microRNA_map[ref] if miRNA.sequence == miRNAs_highexpr[ref][3][2]]  
			if id!=[]:
				miRNAs_highexpr[ref][3].append(''.join(id))
			else:
				del miRNAs_highexpr[ref][3]
		
	if miRNAs_highexpr[ref].has_key(5):
		if miRNAs_highexpr[ref][5][2] not in mats5 and len(miRNAs_highexpr[ref][5][2])>18:
			newSeq = miRNAs_highexpr[ref][5][2]
			newOffset = miRNAs_highexpr[ref][5][1]
			newCount = miRNAs_highexpr[ref][5][0]
			newID = ref+'5N'
			miRNAs_highexpr[ref][5].append(newID)
			microRNA_map[ref].append(MicroRNA(ref, newID, newCount, newOffset, newSeq, findPrime(ref, newOffset)))  #Adds 'N' postfix to denote this is a new mature sequence
		else:
			id = [miRNA.miRNA_id for miRNA in microRNA_map[ref] if miRNA.sequence == miRNAs_highexpr[ref][5][2]] 
			if id!=[]:
				miRNAs_highexpr[ref][5].append(''.join(id))
			else:
				del miRNAs_highexpr[ref][5]
	
def alignShortreads():
	"""
	To align short reads relative to its reference's most expressed mature sequence for the same strand.
	"""
	global shortreads, shortread_map
	f = open('noexprs.txt','w')
	
	for ref in hairpin_map:
		mostExpr = 0
		if miRNAs_highexpr[ref].has_key(3) and miRNAs_highexpr[ref].has_key(5):
			if miRNAs_highexpr[ref][3][0]>miRNAs_highexpr[ref][5][0]:
				mostExpr = 3
			else:
				mostExpr = 5
		elif miRNAs_highexpr[ref].has_key(3):
			mostExpr = 3
		elif miRNAs_highexpr[ref].has_key(5):
			mostExpr = 5
		else:
			#print('Could not find most expressed strand for %s' % (ref))
			#print(miRNAs_highexpr[ref])
			f.write(ref+'\n')
			continue
		for miRNA in microRNA_map[ref]:
			miRNA.setHighestPrime(mostExpr)
		
		removed = 0
		for i in range(len(shortreads[ref])):
			read = i - removed
			srOffset = shortreads[ref][read][0]
			srSeq = shortreads[ref][read][1]
			srEnd = srOffset+len(srSeq)
			newPrime = findPrime(ref, srOffset)
			
			if not miRNAs_highexpr[ref].has_key(newPrime): 
				continue
			
			
			srCount = shortreads[ref][read][2]
			matOffset = int(miRNAs_highexpr[ref][newPrime][1])
			matEnd = matOffset+len(miRNAs_highexpr[ref][newPrime][2])
			diffStart = srOffset - matOffset
			diffEnd = srEnd-matEnd
			
			# Check if actually a MoR
			if (newPrime==5 and ((srEnd - matOffset)<3)) or (newPrime==3 and ((matEnd - srOffset)<3)):
				mors[ref].append(shortreads[ref][read])
				del shortreads[ref][read]
				removed += 1
				if len(shortreads[ref])==0:
					del shortreads[ref]
					break
				continue			
		
			pos = 'start' if newPrime==3 and abs(diffStart) < abs(diffEnd) or newPrime==5 and abs(diffStart) < abs(diffEnd) else 'end'
			diff = diffEnd if pos=='end' else diffStart
			shortread_map[ref].append(Shortread(ref, srOffset, srSeq, srCount, miRNAs_highexpr[ref][newPrime][3], pos, newPrime, diff, newPrime==mostExpr))
			shortreads[ref][read].extend([miRNAs_highexpr[ref][newPrime][3], newPrime==mostExpr, pos, diff, newPrime])
	f.close()
	
	
def isLoop(seq, i):
	chrs = seq[i+1:].replace('.', '')
	if chrs == '': return False
	return True if chrs[0] == ')' else False

def findPrime(ref, offset):
	"""
	Help method for finding what prime the sequence originates from.
	"""
	try:
		seq = hairpin_map[ref].foldstructure
	except IndexError:
		print ('Could not obtain sequence for ref %s, exiting ' % ref)
		return -1
	loops = list()

	for i in range(len(seq)):
		if seq[i] == '(' and isLoop(seq, i):
			i5 = i
			i3 = seq[i:].find(')') + i
			mid = math.ceil((i3+i5)/2.0)
			loops.append(mid)
	
	if len(loops)==0:
		print('Could not find prime as loops is empty for %s' % ref)
		return -1
	
	if len(loops)>1:
		med = len(seq)/2
		mid = min(loops, key=lambda x:abs(x-med))
	else:
		mid = loops[0]

	if offset>mid: return 3
	if offset<=mid: return 5
	
	
def removeUnexpressed(name):
	"""
	To remove short read statistics for reference hairpins not expressed in sample data.
	"""
	global shortreads, shortread_map
	unexpressed = []
	#f = open(name+'_unexpressed_miRNAs.txt', 'w')
	
	for ref in hairpin_map:
		expressed = False
				
		for miRNA in microRNA_map[ref]:
			if miRNA.miRNA_id[-1:]=='N': 
				expressed = True
				continue
								
			bucket = len(miRNA.sequence)//5
			offset = miRNA.offset
			i = max(0, offset-1)
			j = max(offset+1, len(miRNA.sequence)-1)
			
			a = int(miRNA_total[ref][bucket][i])
			b = int(miRNA_total[ref][bucket][offset])
			c = int(miRNA_total[ref][bucket][j])
			
			if (a!=0 or b!=0 or c!=0) and ref in shortread_map:
				expressed = True
			
		if not expressed:
			count = 0.0
			for prime in miRNAs_highexpr[ref]:
				count += float(miRNAs_highexpr[ref][prime][0])
			value = [count, ref]
			unexpressed.append(value)
			del shortreads[ref]
			del shortread_map[ref]
			for miRNA in microRNA_map[ref]:
				miRNA.setUnexpressed()
	
	sort = sorted(unexpressed, reverse=True)
	print 'Number of unexpressed is: %d' % len(unexpressed)
	#for i in range(len(sort)):
		#f.write(str(sort[i][0]) +' '+ str(sort[i][1]) + ' \n')
	#f.close()
	
def findNoReads():
	"""shortreads[ref]=[offset, sequence, normalized count, newID, belongs to highest expressed, start/end alignments, prime]"""
	noSRs = dict()

	for ref in microRNA_map:
		hasShortie = False
		count = 0.0
		for miRNA in microRNA_map[ref]:
			count += miRNA.count
			if miRNA.expressed:
				for Shortread in shortread_map[ref]:
					if Shortread.mature_id==miRNA.miRNA_id:
						hasShortie = True
		if not hasShortie and count>0.0:
			noSRs[ref] = count

	print 'len of noSRs is %d' % len(noSRs)

	return noSRs
