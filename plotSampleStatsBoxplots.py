import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plot
import numpy as np
import shortreadProcesser as sp
import math

def main():
	#font = {'family' : 'normal',
    #    'weight' : 'normal',
    #    'size'   : 40}
	#mpl.rc('font', **font)
	#plot.rcParams['xtick.major.pad']='8'
	#plot.rcParams['ytick.major.pad']='8'

	ago1_sr = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797059_alignments.txt', 'SRR797059')
	ago2_sr = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797060_alignments.txt', 'SRR797060')
	ago3_sr = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797061_alignments.txt', 'SRR797061')
	ago4_sr = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797062_alignments.txt', 'SRR797062')
	ctrl_sr = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797063_alignments.txt', 'SRR797063')

	#Total
	start, end, labels = createData([ago1_sr, ago2_sr, ago3_sr, ago4_sr, ctrl_sr])
	
	#Individual
	start, end, labels = createData([ago2_sr])
	ind = np.arange(len(labels))
	width = 0.2

	fig = plot.figure()
	ax = fig.add_subplot(111)
	bp_start = ax.boxplot(start, positions=ind, widths = width, patch_artist=True, sym='o')
	bp_end = ax.boxplot(end, positions=ind+width, widths=width, patch_artist=True, sym='o')
	
	for box in bp_start['boxes']:
		box.set(color='#2166ac', facecolor='#92c5de')
	for box in bp_end['boxes']:
		box.set(facecolor='#f4a582', color='#b2182b')

	for whisk in bp_start['whiskers']:
		whisk.set(color='#2166ac', linewidth=2)
	for whisk in bp_end['whiskers']:
		whisk.set(color='#b2182b', linewidth=2)

	for median in bp_start['medians']:
		median.set(color='#2166ac', linewidth=2)
	for median in bp_end['medians']:
		median.set(color='#b2182b', linewidth=2)

	for cap in bp_start['caps']:
		cap.set(color='#2166ac', linewidth=2)
	for cap in bp_end['caps']:
		cap.set(color='#b2182b', linewidth=2)

	for sym in bp_start['fliers']:
		sym.set(color='#2166ac', alpha=0.6)
	for sym in bp_end['fliers']:
		sym.set(color='#b2182b', alpha=0.6)

	ax.set_xticks(ind+width*0.5, minor=False)
	ax.set_xticklabels(labels, minor=False, fontdict=False)
	ax.tick_params(axis='y')

	#ax.legend([bp_start, bp_end],['Start','End'])
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	ax.set_ylabel('log$_2$ Shortread counts')

	startRect = mpl.patches.Rectangle([0.025, 0.05], 0.05, 0.1, ec='#2166ac', fc='#92c5de')
	endRect = mpl.patches.Rectangle([0.025, 0.05], 0.05, 0.1, ec='#b2182b', fc='#f4a582')
	ax.legend([startRect, endRect], ['Start', 'End'])
	fig.suptitle('Ago2 quartile shortread alignment offsets', fontsize=20)
	fig.savefig('Ago2_SRdistribution.png')
	plot.clf()

def createData(shortread_maps):
	stat = {}
	labels, start, end = [], [], []

	for shortread_map in shortread_maps:
		for ref in shortread_map:
			for Shortread in shortread_map[ref]:
				# If variation stability is needed, use logarithm of counts with base 2
				count = math.log(Shortread.count, 2)
				if Shortread.position=='start':
					offset = Shortread.mat_offset
					if abs(offset)>2:
						offset='other'
					if offset in stat:
						stat[offset][0].append(count)
					else:
						stat[offset]=[[count], []]
				elif Shortread.position=='end':
					offset = Shortread.mat_offset
					if abs(offset)>2:
						offset = 'other'
					if offset in stat:
						stat[offset][1].append(count)
					else:
						stat[offset]=[[],[count]]

	for key, value in iter(sorted(stat.iteritems())):
		start.append(value[0])
		end.append(value[1])
		labels.append(key)

	return start, end, labels



if __name__ == '__main__':
	main()

