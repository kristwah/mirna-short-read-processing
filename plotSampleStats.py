import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np 
import shortreadProcesser as sp

def main():
	#ago1_sr = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797059_alignments.txt', 'SRR797059')
	#ago2_sr = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797060_alignments.txt', 'SRR797060')
	#ago3_sr = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797061_alignments.txt', 'SRR797061')
	ago4_sr = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797062_alignments.txt', 'SRR797062')
	#ctrl_sr = sp.produceShortreads('hsa_reference_new.txt', 'hsa_folds_new.txt', 'hsa_matures_new.txt', 'SRR797063_alignments.txt', 'SRR797063')

	#m1_sr = sp.produceShortreads('mmu_references_new.txt', 'mmu_folds_new.txt', 'mmu_matures.txt', 'miRNA01_alignments.txt', 'miRNA01')
	#m2_sr = sp.produceShortreads('mmu_references_new.txt', 'mmu_folds_new.txt', 'mmu_matures.txt', 'miRNA02_alignments.txt', 'miRNA02')
	#m3_sr = sp.produceShortreads('mmu_references_new.txt', 'mmu_folds_new.txt', 'mmu_matures.txt', 'miRNA03_alignments.txt', 'miRNA03')
	#m4_sr = sp.produceShortreads('mmu_references_new.txt', 'mmu_folds_new.txt', 'mmu_matures.txt', 'miRNA04_alignments.txt', 'miRNA04')
	#m5_sr = sp.produceShortreads('mmu_references_new.txt', 'mmu_folds_new.txt', 'mmu_matures.txt', 'miRNA05_alignments.txt', 'miRNA05')
	#m6_sr = sp.produceShortreads('mmu_references_new.txt', 'mmu_folds_new.txt', 'mmu_matures.txt', 'miRNA06_alignments.txt', 'miRNA06')

	#m7_sr = sp.produceShortreads('mmu_references_new.txt', 'mmu_folds_new.txt', 'mmu_matures.txt', 'miRNA07_alignments.txt', 'miRNA07')
	#m8_sr = sp.produceShortreads('mmu_references_new.txt', 'mmu_folds_new.txt', 'mmu_matures.txt', 'miRNA08_alignments.txt', 'miRNA08')
	#m9_sr = sp.produceShortreads('mmu_references_new.txt', 'mmu_folds_new.txt', 'mmu_matures.txt', 'miRNA09_alignments.txt', 'miRNA09')
	#m10_sr = sp.produceShortreads('mmu_references_new.txt', 'mmu_folds_new.txt', 'mmu_matures.txt', 'miRNA10_alignments.txt', 'miRNA10')
	#m11_sr = sp.produceShortreads('mmu_references_new.txt', 'mmu_folds_new.txt', 'mmu_matures.txt', 'miRNA11_alignments.txt', 'miRNA11')
	#m12_sr = sp.produceShortreads('mmu_references_new.txt', 'mmu_folds_new.txt', 'mmu_matures.txt', 'miRNA12_alignments.txt', 'miRNA12')

	#docKOlabels, docKOstart, docKOend = generateListsBar([m1_sr, m2_sr, m3_sr])
	#docWTlabels, docWTstart, docWTend = generateListsBar([m4_sr, m5_sr, m6_sr])
	#docLabels, docStart, docEnd = generateListsBar([m1_sr, m2_sr, m3_sr, m4_sr, m5_sr, m6_sr])
	#ghKOlabels, ghKOstart, ghKOend = generateListsBar([m7_sr, m8_sr, m9_sr])
	#ghWTlabels, ghWTstart, ghWTend = generateListsBar([m10_sr, m11_sr, m12_sr])
	#ghLabels, ghStart, ghEnd = generateListsBar([m7_sr, m8_sr, m9_sr, m10_sr, m11_sr, m12_sr])
	#gcfLabels, gcfStart, gcfEnd = generateListsBar([m1_sr, m2_sr, m3_sr, m4_sr, m5_sr, m6_sr,m7_sr, m8_sr, m9_sr, m10_sr, m11_sr, m12_sr])
	
	#gcfKOlabels, gcfKOstart, gcfKOend = generateListsBar([m1_sr, m2_sr, m3_sr, m7_sr, m8_sr, m9_sr])
	#gcfWTlabels, gcfWTstart, gcfWTend = generateListsBar([m4_sr, m5_sr, m6_sr, m10_sr, m11_sr, m12_sr])

	#ago1labels, ago1start, ago1end = generateListsBar([ago1_sr])
	#ago2labels, ago2start, ago2end = generateListsBar([ago2_sr])
	#ago3labels, ago3start, ago3end = generateListsBar([ago3_sr])
	ago4labels, ago4start, ago4end = generateListsBar([ago4_sr])

	#GSElabels, GSEdataStart, GSEdataEnd = generateListsBar([ago1_sr, ago2_sr, ago3_sr, ago4_sr, ctrl_sr])

	#plot(docKOlabels, docKOstart, docKOend, 'DOC KO', 'DOC_KO')
	#plot(docWTlabels, docWTstart, docWTend, 'DOC WT', 'DOC_WT')
	#plot(docLabels, docStart, docEnd, 'DOC', 'DOC')
	#plot(ghKOlabels, ghKOstart, ghKOend, 'GH KO', 'GH_KO')
	#plot(ghWTlabels, ghWTstart, ghWTend, 'GH WT', 'GH_WT')
	#plot(ghLabels, ghStart, ghEnd, 'GH', 'GH')
	#plot(gcfLabels, gcfStart, gcfEnd, 'GCF', 'GCF')

	#plot(ago1labels, ago1start, ago1end, 'Ago1', 'Ago1')
	#plot(ago2labels, ago2start, ago2end, 'Ago2', 'Ago2')
	#plot(ago3labels, ago3start, ago3end, 'Ago3', 'Ago3')
	plot(ago4labels, ago4start, ago4end, 'Ago4', 'Ago4')
	#plot(GSElabels, GSEdataStart, GSEdataEnd, 'GSE45506', 'GSE45506')

	#plot(gcfKOlabels, gcfKOstart, gcfKOend, 'GCF KO', 'GCF KO')
	#plot(gcfWTlabels, gcfWTstart, gcfWTend, 'GCF WT', 'GCF WT')

def plot(labels, start, end, title, name):
	#font = {'family' : 'normal',
    #    'weight' : 'normal',
    #    'size'   : 50}
	#mpl.rc('font', **font)

	width = 0.4

	ind = np.arange(11)

	fig = plt.figure()
	
	ax = fig.add_subplot(111)
	try:
		ax.bar(ind, start, width, color='#92c5de', edgecolor='#2166ac', label='Start offset')
	 	ax.bar(ind, end, width, color='#f4a582', edgecolor='#b2182b', label='End offset', bottom=start)
	except:
		return

	ax.legend(loc='upper left')
	ax.set_xticks(ind+0.5*width, minor=False)
	ax.set_xticklabels(labels, minor=False, fontdict=False)
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	ax.set_ylabel = 'Shortreads'
	
	plt.title(title+' short read alignments')
	plt.savefig(name+'_alignment_offsets.png')
	plt.clf()

def generateListsBar(shortread_maps):
	"""
	Generete a start and end list where every offset outside [0,2] is 'others'. Starts are [0,1,2,others], ends are [others, 2, 1, 0].
	"""
	stat = {}
	labels, start, end = [], [], []

	for shortread_map in shortread_maps:
		for ref in shortread_map:
			for Shortread in shortread_map[ref]:
				#if Shortread.prime==5: continue
				if Shortread.position=='start':
					offset = Shortread.mat_offset
					count = Shortread.count
					if offset not in range(-2,3):
						offset='other'
					if offset in stat:
						stat[offset][0] += count
					else:
						stat[offset]=[count, 0]
				elif Shortread.position=='end':
					offset = Shortread.mat_offset
					count = Shortread.count
					if offset not in range(-2,3):
						offset = 'other'
					if offset in stat:
						stat[offset][1] += count
					else:
						stat[offset]=[0,count]

	for key, value in iter(sorted(stat.iteritems())):
		start.append(value[0])
		end.append(value[1])
		labels.append(key)

	print start
	print end
	print labels

	start.extend([0,0,0, 0, 0])
	end.extend([0,0,0,0,0])
	end.reverse()

	print start
	print end
	#print labels
	labels = ['-2', '-1', '0', '1', '2', 'others', '2', '1', '0', '-1', '-2']
	print labels

	return labels, start, end

def generateListBox(shortread_maps):
	stat = {}
	labels, start, end = [], [], []

if __name__ == '__main__':
	main()
